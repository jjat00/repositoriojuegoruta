/**
 * Archivo: PrincipalJuegoRuta.java
 * Fecha: Abril de 2019
 * @author Jaimen Aza, cod 1923556
 * @author Valentina Salamanca, cod 1842427
 */
package juegoRuta;

import java.util.LinkedList;
import java.util.Collections;

/**
 * The Class Baraja. Clase que maneja los procesos en la clase GUIJuegoRuta.
 */
public class Baraja {

    /**
     * Crea las listas de cartas (el objeto cartas).
     */

    /** The baraja. */
    private LinkedList<Cartas> baraja = new LinkedList<Cartas>();

    /** The cartasJugador. */
    private LinkedList<Cartas> cartasJugador = new LinkedList<Cartas>();

    /** The cartasComputador. */
    private LinkedList<Cartas> cartasComputador = new LinkedList<Cartas>();

    /** The cartasDescartadas. */
    private LinkedList<Cartas> cartasDescartadas = new LinkedList<Cartas>();

    /** The cartasMesaJugador. */
    private LinkedList<Cartas> cartasMesaJugador = new LinkedList<Cartas>();

    /** The cartasMesaComputador. */
    private LinkedList<Cartas> cartasMesaComputador = new LinkedList<Cartas>();

    /**
     * Constructor de la clase Baraja.
     */
    public Baraja() {
        /** Agrega las cartas de tipo distancia. */
        for (int index = 0; index < 10; index++) {
            baraja.add(new Cartas("Distancia", "25"));
            baraja.add(new Cartas("Distancia", "50"));
        }
        for (int index = 0; index < 12; index++) {
            baraja.add(new Cartas("Distancia", "75"));
            baraja.add(new Cartas("Distancia", "100"));
        }
        for (int index = 0; index < 4; index++) {
            baraja.add(new Cartas("Distancia", "200"));
        }
        /** Agrega las cartas de tipo solucion. */
        for (int index = 0; index < 6; index++) {
            baraja.add(new Cartas("Soluciones", "GASOLINA"));
            baraja.add(new Cartas("Soluciones", "FINLIMITE"));
            baraja.add(new Cartas("Soluciones", "RUEDARECAMBIO"));
            baraja.add(new Cartas("Soluciones", "REPARACIONES"));
        }
        for (int index = 0; index < 14; index++) {
            baraja.add(new Cartas("Soluciones", "SIGA"));
        }
        /** Agrega las cartas de tipo poder. */
        for (int index = 0; index < 1; index++) {
            baraja.add(new Cartas("Poder", "ASDELVOLANTE")); //no puede tener accidentes
            baraja.add(new Cartas("Poder", "CISTERNA")); // no puede tener sin gasolina
            baraja.add(new Cartas("Poder", "IMPINCHABLE")); // no puede tener pinchazo
            baraja.add(new Cartas("Poder", "VIALIBRE")); // no puede tener semaforos rojos ni limtes velocidad
        }
        /** Agrega las cartas de tipo ataque. */
        for (int index = 0; index < 3; index++) {
            baraja.add(new Cartas("Ataque", "ACCIDENTE"));
            baraja.add(new Cartas("Ataque", "PINCHAZO"));
            baraja.add(new Cartas("Ataque", "SINGASOLINA"));
        }
        for (int index = 0; index < 4; index++) {
            baraja.add(new Cartas("Ataque", "LIMITEVELOCIDAD"));
        }
        for (int index = 0; index < 5; index++) {
            baraja.add(new Cartas("Ataque", "PARE"));
        }
    }
    
    /**
     * Metodo para imprimir los nombres de las cartas de la baraja
     * 
     */
    public void imprimirCartas() {
        for (int index = 0; index < baraja.size(); index++) {
            System.out.println(baraja.get(index).getTipoCarta());
        }
        System.out.println(baraja.size());
    }

    /**
     * Metodo para barajar.
     * 
     */
    public void barajar() {
        Collections.shuffle(baraja);
    }

    /**
     * Metodo para dar las cartas al jugador
     * 
     * @param int numeroCartas.
     */
    public void darCartasJugador(int numeroCartas) {
        for (int index = 0; index < numeroCartas; index++) {
            cartasJugador.add(baraja.get(index));
            //System.out.println(baraja.get(index));
            baraja.remove(index);
        }
    }

    /**
     * Metodo para darle una carta al jugagor una carta de la baraja (cuando va a
     * comer una carta)
     * 
     */
    public void darCartasJugador() {
        cartasJugador.add(baraja.getFirst());
        baraja.removeFirst();
    }

    /**
     * Metodo para dar las cartas al computador.
     * 
     * @param int numeroCartas.
     */
    public void darCartasComputador(int numeroCartas) {
        for (int index = 0; index < numeroCartas; index++) {
            cartasComputador.add(baraja.get(index));
            //System.out.println(baraja.get(index));
            baraja.remove(index);
        }
    }

    /**
     * Metodo para darle una carta al computador una carta de la baraja (cuando va a
     * comer una carta)
     * 
     */
    public void darCartasComputador() {
        //computadorCogioCarta = true;
        cartasComputador.add(baraja.getFirst());
        baraja.removeFirst();
    }



	/**
     * Metodo para obtener la baraja
     * 
     * @param LinkedLits<Cartas>
     */
    public LinkedList<Cartas> obtenerBaraja() {
        return baraja;
    }

    public void devolverCartas() {
        if (obtenerBaraja().size() == 0) {
            for (int index = 0; index < cartasMesaJugador.size(); index++) {
                baraja.add(cartasMesaJugador.get(index));
            }
            for (int index = 0; index < baraja.size() - 1; index++) {
                cartasMesaJugador.removeFirst();
            }
        }

    }

    /**
     * Metodo para poner una carta en la zona del juego del jugador.
     * 
     * @param int cualCarta.
     * @param int cualJugador.
     */
    public void ponerCartaEnMesaJugador(int cualCarta, int cualJugador) {
        if (cualJugador == 0) {
            for (int index = 0; index < cartasComputador.size(); index++) {
                if (index == cualCarta) {
                    cartasMesaJugador.add(cartasComputador.get(index));
                    cartasComputador.remove(index);
                }
            }
        }
        if (cualJugador == 1) {
            for (int index = 0; index < cartasJugador.size(); index++) {
                if (index == cualCarta) {
                    cartasMesaJugador.add(cartasJugador.get(index));
                    cartasJugador.remove(index);
                }
            }
        }

    }

    /**
     * Metodo para poner una carta en la zona del juego del computador.
     * 
     * @param int cualCarta.
     * @param int cualJugador.
     */
    public void ponerCartaEnMesaComputador(int cualCarta, int cualJugador) {
        if (cualJugador == 0) {
            for (int index = 0; index < cartasComputador.size(); index++) {
                if (index == cualCarta) {
                    cartasMesaComputador.add(cartasComputador.get(index));
                    cartasComputador.remove(index);
                }
            }
        }
        if (cualJugador == 1) {
            for (int index = 0; index < cartasJugador.size(); index++) {
                if (index == cualCarta) {
                    cartasMesaComputador.add(cartasJugador.get(index));
                    cartasJugador.remove(index);
                }
            }
        }
    }


    /**
     * Metodo para que el jugador pueda poner una carta en la zona de las cartas
     * descartadas.
     * 
     * @param int cualCarta.
     */
    public void descartarCartaJugador(int cualCarta) {
        for (int index = 0; index < cartasJugador.size(); index++) {
            if (index == cualCarta) {
                cartasDescartadas.add(cartasJugador.get(index));
                cartasJugador.remove(index);
            }
        }
    }


	/**
     * Metodo para que el computador pueda poner una carta en la zona de las cartas
     * descartadas.
     * 
     */
    public void descartarCartaComputador() {
        cartasDescartadas.add(cartasComputador.getFirst());
        cartasComputador.removeFirst();
    }

    /**
     * Metodo para obtener las cartas del jugador.
     * 
     * @return cartasJugador.
     */
    public LinkedList<Cartas> obtenerCartasJugador() {
        return cartasJugador;
    }


	/**
     * Metodo para obtener las cartas del computador.
     * 
     * @return cartasComputador.
     */
    public LinkedList<Cartas> obtenerCartasComputador() {
        return cartasComputador;
    }


	/**
     * Metodo para obtener las cartas disponibles.
     * 
     * @return baraja.
     */
    public LinkedList<Cartas> obtenerCartasDisponibles() {
        return baraja;
    }


	/**
     * Metodo para obtener las cartas de la zona del juego del jugador.
     * 
     * @return cartasMesaJugador.
     */
    public LinkedList<Cartas> obtenerCartasMesaJugador() {
        return cartasMesaJugador;
    }

    /**
     * Metodo para obtener las cartas de la zona del juego del computador.
     * 
     * @return cartasMesaComputador.
     */
    public LinkedList<Cartas> obtenerCartasMesaComputador() {
        return cartasMesaComputador;
    }


	/**
     * Metodo para obtener la carta que se comio.
     * 
     * @return carta.
     */
    public Cartas obtenerCartaDisponible() {
        Cartas carta = baraja.getFirst();
        baraja.removeFirst();
        return carta;
    }


	/**
     * Metodo para obtener la ultima carta que han puesto en la zona de cartas
     * descartadas.
     * 
     * @return cartasDescartadas.getLast().
     */
    public Cartas obtenerCartaDescartada() {
        return cartasDescartadas.getLast();
    }


}

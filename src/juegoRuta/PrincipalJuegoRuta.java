package juegoRuta;
import java.awt.EventQueue;
import javax.swing.UIManager;

public class PrincipalJuegoRuta {
	public static void main(String[] args) {
		try {
			String className = UIManager.getCrossPlatformLookAndFeelClassName();
			UIManager.setLookAndFeel(className);
		} catch (Exception e) {
		}

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				GUIJuegoRuta gui = new GUIJuegoRuta();
			}
		});
	}
}

/**
 * Archivo: PrincipalJuegoRuta.java
 * Fecha: Abril de 2019
 * @author Jaimen Aza, cod 1923556
 * @author Valentina Salamanca, cod 1842427
 */
package juegoRuta;

/**
 * The Class Cartas.
 */
public class Cartas {

    /** The nombreCarta. */
    private String nombreCarta;

    /** The tipoCarta. */
    private String tipoCarta;

    /**
     * Inicializa el tipo de la carta con su respectivo nombre
     * 
     * @param tipoCarta   the tipoCarta.
     * @param nombreCarta the nombreCarta
     */
    public Cartas(String tipoCarta, String nombreCarta) {
        this.tipoCarta = tipoCarta;
        this.nombreCarta = nombreCarta;
    }

    /**
     * Metodo que obtiene el tipo de carta (Ataques, soluciones, poderes o
     * distancia)
     * 
     * @return tipoCarta.
     */
    public String getTipoCarta() {
        return this.tipoCarta;
    }

    /**
     * Metodo que obtiene el nombre de la carta
     * 
     * @return nombreCarta.
     */
    public String getNombreCarta() {
        return this.nombreCarta;
    }
}

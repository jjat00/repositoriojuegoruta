package juegoRuta;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.EventObject;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.Border;
import javax.swing.ButtonGroup;

public class GUIJuegoRuta extends JFrame {
    private Container containerZonaJuego;
    private JPanel panelCartasComputador;
    private JPanel panelCartasJugador;
    private JPanel panelZonaJuegoJugador;
    private JPanel panelZonaJuegoComputador;
    private JPanel panelCartasDisponibles;
    private JPanel panelCartasDescartadas;
    private JLabel labelCartasComputador[];
    private JLabel labelCartasJugador[];
    private JLabel labelZonaJuegoComputador[];
    private JLabel labelZonaJuegoJugador[];
    private JLabel labelCartasDisponibles;
    private JLabel labelCartasDescartadas;
    private JLabel areaMensajesJuego;
    private EscuchaZonaJuego escuchaCartas;
    private JComboBox comboBoxLugarCarta;
    private JScrollPane scrollZonaJuegoJugador;
    private JScrollPane scrollZonaJuegoComputador;
    private JScrollPane scrollCartasJugador;
    private JScrollPane scrollCartasComputador;

    private Baraja baraja;
    private ControlJuegoRuta control;
    private final Color colorMesa = new Color(68, 151, 219);
    private final Border borde = BorderFactory.createLineBorder(Color.BLACK, 2);
    private GridBagConstraints constraints;
    private JPanel panelPuntuacionComputador;
    private JPanel panelPuntuacionJugador;

    public GUIJuegoRuta() {
        super("Ruta Game");
        inicializarVentanaJuego();
        // Set default window configuration
        this.setUndecorated(false);
        this.setPreferredSize(new Dimension(1300, 700));
        this.pack();
        this.setLocationRelativeTo(null);
        this.setResizable(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public void inicializarVentanaJuego() {
        // JFrame container zona configuracion del juego and layout
        this.containerZonaJuego = getContentPane();
        this.containerZonaJuego.setLayout(new GridBagLayout());
        this.containerZonaJuego.setBackground(colorMesa);

        constraints = new GridBagConstraints();

        escuchaCartas = new EscuchaZonaJuego();
        control = new ControlJuegoRuta();


        panelCartasJugador = new JPanel();
        panelCartasComputador = new JPanel();
        panelCartasDisponibles = new JPanel();
        panelZonaJuegoJugador = new JPanel();
        panelCartasDescartadas = new JPanel();
        panelZonaJuegoComputador = new JPanel();
        panelPuntuacionComputador = new JPanel();
        panelPuntuacionJugador = new JPanel();

        scrollZonaJuegoJugador = new JScrollPane(panelZonaJuegoJugador, JScrollPane.VERTICAL_SCROLLBAR_NEVER, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrollZonaJuegoJugador.setPreferredSize(new Dimension(500, 20));
        scrollZonaJuegoComputador = new JScrollPane(panelZonaJuegoComputador, JScrollPane.VERTICAL_SCROLLBAR_NEVER, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrollZonaJuegoComputador.setPreferredSize(new Dimension(550, 20));

        baraja = new Baraja();
        baraja.barajar();
        //baraja.imprimirCartas();
        baraja.darCartasJugador(6);
        repartirCartasJugador(baraja.obtenerCartasJugador());
        baraja.darCartasComputador(6);
        repartirCartasComputador(baraja.obtenerCartasComputador());
        ponerCartasDisponibles();
        mostrarPuntuacionComputador(0);
        mostrarPuntuacionJugador(0);
        // ponerCartasZonaJuegoJugador();
        mostrarMensaje("Es tu turno, primero debes comer carta");
    }

    /**
     * Metodo para poner un panel en unas condiciones dadas.
     * 
     * @param x               the x
     * @param y               the y
     * @param alto            the alto.
     * @param ancho           the ancho.
     * @param estirarColumnas the estirarColumnas
     * @param estirarFilas    the estirarFilas,
     * @param panel           the panel.
     */
    public void ponerPanel(int x, int y, int alto, int ancho, double estirarColumnas, double estirarFilas, JPanel panel) {
        constraints.gridx = x;
        constraints.gridy = y;
        constraints.gridheight = alto;
        constraints.gridwidth = ancho;
        constraints.weightx = estirarColumnas;
        constraints.weighty = estirarFilas;
        this.containerZonaJuego.add(panel, constraints);
    }

    /**
     * Metodo para poner un scroll en unas condiciones dadas.
     * 
     * @param x      the x
     * @param y      the y
     * @param fill   the GridBagConstrainsts.BOTH.
     * @param scroll the scroll.
     * @param panel  the panel.
     */
    public void ponerScroll(int x, int y, int fill, JScrollPane scroll, JPanel panel) {
        constraints.gridx = x;
        constraints.gridy = y;
        constraints.fill = GridBagConstraints.BOTH;
        this.containerZonaJuego.add(scroll, constraints);
        scrollZonaJuegoComputador.getViewport().setView(panel);
    }
    
    public void mostrarPuntuacionComputador(int puntos) {
        panelPuntuacionComputador.setBackground(colorMesa);
        String mensaje = "Kilometros: " + String.valueOf(puntos)+" km ";
        JLabel puntosComputador = new JLabel(mensaje);
        puntosComputador.setBackground(colorMesa);
        panelPuntuacionComputador.add(puntosComputador);
        ponerPanel(0, 0, 1, 1, 0, 0, panelPuntuacionComputador);
    }

    public void mostrarPuntuacionJugador(int puntos) {
        panelPuntuacionJugador.setBackground(colorMesa);
        String mensaje = "Kilometros: " + String.valueOf(puntos)+" km ";
        JLabel puntosJugador = new JLabel(mensaje);
        puntosJugador.setBackground(colorMesa);
        panelPuntuacionJugador.add(puntosJugador);
        ponerPanel(0, 2, 1, 1, 0, 0, panelPuntuacionJugador);
    }

    public void repartirCartasJugador(final LinkedList<Cartas> cartasJugador) {
        panelCartasJugador.setLayout(new FlowLayout());
        panelCartasJugador.setBorder(borde);
        panelCartasJugador.setBackground(colorMesa);
        labelCartasJugador = new JLabel[cartasJugador.size()];
        for (int index = 0; index < cartasJugador.size(); index++) {
            labelCartasJugador[index] = new JLabel();
            labelCartasJugador[index].setIcon(new ImageIcon("src/cartas/" + cartasJugador.get(index).getNombreCarta() + ".jpg"));
            labelCartasJugador[index].addMouseListener(escuchaCartas);
            panelCartasJugador.add(labelCartasJugador[index]);
        }
        ponerPanel(1, 2 , 1, 1, 1.0, 1.0, panelCartasJugador);
    }

    public void repartirCartasComputador(final LinkedList<Cartas> cartasComputador) {
        panelCartasComputador.setLayout(new FlowLayout());
        panelCartasComputador.setBackground(colorMesa);
        panelCartasComputador.setBorder(borde);
        labelCartasComputador = new JLabel[cartasComputador.size()];
        for (int index = 0; index < cartasComputador.size(); index++) {
            labelCartasComputador[index] = new JLabel();
            //labelCartasComputador[index].setIcon(new ImageIcon("src/cartas/" + cartasComputador.get(index).getNombreCarta() + ".jpg"));
            labelCartasComputador[index].setIcon(new ImageIcon("src/cartas/REVERSO.jpg"));
            labelCartasComputador[index].addMouseListener(escuchaCartas);
            panelCartasComputador.add(labelCartasComputador[index]);
        }
        ponerPanel(1, 0, 1, 1, 1.0, 1.0, panelCartasComputador);
    }

    public void ponerCartasDisponibles() {
        panelCartasDisponibles.setLayout(new FlowLayout());
        panelCartasDisponibles.setBackground(colorMesa);
        panelCartasDisponibles.setBorder(borde);
        labelCartasDisponibles = new JLabel();
        labelCartasDisponibles.setIcon(new ImageIcon("src/cartas/REVERSO.jpg" ));
        //labelCartasDisponibles.setIcon(new ImageIcon("src/cartas/"+ baraja.obtenerCartasDisponibles().getFirst().getNombreCarta()+".jpg" ));
        labelCartasDisponibles.addMouseListener(escuchaCartas);
        panelCartasDisponibles.add(labelCartasDisponibles);
        ponerPanel(1, 1, 1, 1, 1.0, 1.0, panelCartasDisponibles);
    }

    public void ponerCartasDescartadas(final Cartas cartaDescartada) {
        panelCartasDescartadas.setLayout(new FlowLayout());
        panelCartasDescartadas.setBackground(colorMesa);
        panelCartasDescartadas.setBorder(borde);
        labelCartasDescartadas = new JLabel();
        labelCartasDescartadas.setIcon(new ImageIcon("src/cartas/" + cartaDescartada.getNombreCarta() + ".jpg"));
        panelCartasDescartadas.add(labelCartasDescartadas);
        ponerPanel(2, 1, 1, 1, 1.0, 1.0, panelCartasDescartadas);
    }

    public void ponerCartasZonaJuegoJugador(final LinkedList<Cartas> cartasJuegoJugador) {
        panelZonaJuegoJugador.setBorder(borde);
        panelZonaJuegoJugador.setBackground(colorMesa);
        labelZonaJuegoJugador = new JLabel[cartasJuegoJugador.size()];
        for(int index = 0; index < cartasJuegoJugador.size(); index++){
            labelZonaJuegoJugador[index] = new JLabel();
            labelZonaJuegoJugador[index].setIcon(new ImageIcon("src/cartas/" + cartasJuegoJugador.get(index).getNombreCarta() + ".jpg"));
            panelZonaJuegoJugador.add(labelZonaJuegoJugador[index]);
        }
        ponerPanel(2, 2, 1, 1, 1.0, 1.0, panelZonaJuegoJugador);

        scrollZonaJuegoJugador.setPreferredSize(new Dimension(500, 20));
		constraints.gridx = 2; // Las cartas del jugador empieza en la columna cero.
		constraints.gridy = 2; // Las cartas del jugador empieza en la fila cero
		constraints.fill = GridBagConstraints.BOTH;
		this.containerZonaJuego.add(scrollZonaJuegoJugador, constraints);
		scrollZonaJuegoJugador.setViewportView(panelZonaJuegoJugador);
    }

    public void ponerCartasZonaJuegoComputador(final LinkedList<Cartas> cartasJuegoComputador) {
        panelZonaJuegoComputador.setBorder(borde);
        panelZonaJuegoComputador.setBackground(colorMesa);
        labelZonaJuegoComputador = new JLabel[cartasJuegoComputador.size()];
        for(int index = 0; index < cartasJuegoComputador.size(); index++){
            labelZonaJuegoComputador[index] = new JLabel();
            labelZonaJuegoComputador[index].setIcon(new ImageIcon("src/cartas/" + cartasJuegoComputador.get(index).getNombreCarta() + ".jpg"));
            panelZonaJuegoComputador.add(labelZonaJuegoComputador[index]);

        }
        ponerPanel(2, 0, 1, 1, 1.0, 1.0, panelZonaJuegoComputador);

        scrollZonaJuegoComputador.setPreferredSize(new Dimension(550, 20));
        constraints.gridx = 2; // Las cartas del jugador empieza en la columna cero.
        constraints.gridy = 0; // Las cartas del jugador empieza en la fila cero
        constraints.fill = GridBagConstraints.BOTH;
        this.containerZonaJuego.add(scrollZonaJuegoComputador, constraints);
        scrollZonaJuegoComputador.getViewport().setView(panelZonaJuegoComputador);

    }

    /**
     * Actualizar panel jugador.
     */
    public void actualizarPanelJugador() {
        panelCartasJugador.removeAll();
        repartirCartasJugador(baraja.obtenerCartasJugador());
        panelCartasJugador.updateUI();
    }

    public void actualizarPanelComputador() {
        panelCartasComputador.removeAll();
        repartirCartasComputador(baraja.obtenerCartasComputador());
        panelCartasComputador.updateUI();
    }

    public void actualizarPanelCartasDisponibles() {
        panelCartasDisponibles.removeAll();
        ponerCartasDisponibles();
        panelCartasDisponibles.updateUI();
    }

    public void actualizarPanelCartaZonaJuegoJugador() {
        panelZonaJuegoJugador.removeAll();
        ponerCartasZonaJuegoJugador(baraja.obtenerCartasMesaJugador());
        panelZonaJuegoJugador.updateUI();
    }

    public void actualizarPanelCartasDescartadas() {
        panelCartasDescartadas.removeAll();
        ponerCartasDescartadas(baraja.obtenerCartaDescartada());
        panelCartasDescartadas.updateUI();
    }

    public void actualizarPanelCartaZonaJuegoComputador() {
        panelZonaJuegoComputador.removeAll();
        ponerCartasZonaJuegoComputador(baraja.obtenerCartasMesaComputador());
        panelZonaJuegoComputador.updateUI();
    }

    public String seleccionarLugarCarta() {
        final JPanel panel = new JPanel();
        final Object[] lugarCarta = {"Juego", "Descarte", "Oponente" };
        comboBoxLugarCarta = new JComboBox(lugarCarta);
        //comboBoxLugarCarta.setSelectedIndex(0);
        panel.add(comboBoxLugarCarta);
        JOptionPane.showMessageDialog(null, panel, "Seleccionar lugar carta", JOptionPane.QUESTION_MESSAGE);
        final String cualLugar = comboBoxLugarCarta.getSelectedItem().toString();
        return cualLugar;
    }

    public void mostrarMensaje(final String mensaje) {
        final JPanel panelMenaje = new JPanel();
        final JLabel labelMenaje = new JLabel(mensaje);
        panelMenaje.add(labelMenaje);
        JOptionPane.showMessageDialog(null, panelMenaje, "Advertencia!!!", JOptionPane.WARNING_MESSAGE);
    }

    public void descartarCarta(){
        
        baraja.descartarCartaComputador();
        actualizarPanelCartasDescartadas();
        actualizarPanelComputador();
        control.yaJugoComputador();
        control.comerCarta(false);
    }

    public void responderComputador(final String cartaSolución, String cartaPoder, int cualCarta) {
        boolean movimiento = true;
        for (int index1 = 0; index1 < baraja.obtenerCartasComputador().size(); index1++) {
            if (cualCarta == 3) {
                if (baraja.obtenerCartasComputador().get(index1).getNombreCarta() == "25" || baraja.obtenerCartasComputador().get(index1).getNombreCarta() == "50") {
                    movimiento = false;
                    sumarPuntosComputador(Integer.valueOf(baraja.obtenerCartasComputador().get(index1).getNombreCarta()));
                    baraja.ponerCartaEnMesaComputador(index1, 0);
                    actualizarPanelCartaZonaJuegoComputador();
                    actualizarPanelComputador();
                    control.yaJugoComputador();
                    control.comerCarta(false);
                    control.seguirJuegoComputador(true);
                    break;
                }
            }
            else if (baraja.obtenerCartasComputador().get(index1).getNombreCarta() == cartaSolución || baraja.obtenerCartasComputador().get(index1).getNombreCarta() == cartaPoder) {
                movimiento = false;
                
                if (baraja.obtenerCartasComputador().get(index1).getNombreCarta() == cartaPoder) {
                    sumarPuntosComputador(300);
                }
                baraja.ponerCartaEnMesaComputador(index1, 0);
                actualizarPanelCartaZonaJuegoComputador();
                actualizarPanelComputador();
                control.yaJugoComputador();
                control.comerCarta(false);
                control.seguirJuegoComputador(true);
                break;
            }
        }
        final String carta = baraja.obtenerCartasComputador().getFirst().getTipoCarta();
        if (carta == "Ataque") {
            
            baraja.ponerCartaEnMesaJugador(0, 0);
            actualizarPanelComputador();
            actualizarPanelCartaZonaJuegoJugador();
            control.yaJugoComputador();
            control.comerCarta(false);
            control.seguirJuegoComputador(true);
        } else {
            if (movimiento) {
                descartarCarta();
            }
        }
        actualizarPanelComputador();
    }

    public void ponerCartaEnMesaComputador(int index){
        baraja.ponerCartaEnMesaComputador(index, 1);
        actualizarPanelCartaZonaJuegoComputador();
        actualizarPanelComputador();
        actualizarPanelJugador();
        control.yaJugoJugador();
    }

    public void ponerCartaEnMesaJugador(int index){
        baraja.ponerCartaEnMesaJugador(index, 1);
        actualizarPanelCartaZonaJuegoJugador();
        actualizarPanelJugador();
        control.yaJugoJugador();
        control.seguirJuegoJugador(true);
    }

    public void ponerCartaEnMesaJugador() {
        
        baraja.ponerCartaEnMesaJugador(0, 0);
        actualizarPanelComputador();
        actualizarPanelCartaZonaJuegoJugador();
        control.yaJugoComputador();
        control.comerCarta(false);
        control.seguirJuegoComputador(true);
    }

    public void sumarPuntosJugador(int puntos) {
        int puntosJugador = control.sumarKmJugador(puntos);
        panelPuntuacionJugador.removeAll();
        mostrarPuntuacionJugador(puntosJugador);
        panelPuntuacionJugador.updateUI();
        if (control.estadokmJugador()) {
            mostrarMensaje("El jugador Ganó");
        }
    }

    public void sumarPuntosComputador(int puntos){
        int puntosComputador = control.sumarKmComputador(puntos);
        panelPuntuacionComputador.removeAll();
        mostrarPuntuacionComputador(puntosComputador);
        panelPuntuacionComputador.updateUI();
        if (control.estadokmComputador()) {
            mostrarMensaje("El Computador Ganó !!!");
        }
    }

    private class EscuchaZonaJuego extends MouseAdapter {
        public void mouseClicked(final MouseEvent event) {
            int cantidadCartasDisponibles = baraja.obtenerCartasDisponibles().size();
            if (cantidadCartasDisponibles == 0) {
                if (control.totalPuntosJugador() > control.totalPuntosComputador()) {
                    mostrarMensaje("El jugador Ganó");
                } else {
                    mostrarMensaje("El Computador Ganó");
                }
            }
            if (control.turnoJugador()) {
                if (!control.yaComioCarta()) {
                    if (event.getSource() == labelCartasDisponibles) {
                        baraja.darCartasJugador();
                        actualizarPanelJugador();
                        actualizarPanelCartasDisponibles();
                        control.comerCarta(true);
                        //System.out.println(control.yaComioCarta());
                    }
                }
                if (control.yaComioCarta()) {
                    for (int index = 0; index < baraja.obtenerCartasJugador().size(); index++) {
                        if (event.getSource() == labelCartasJugador[index]) {
                            final boolean empezarJuego = control.empezarJuego(baraja.obtenerCartasJugador().get(index).getNombreCarta());
                            final boolean seguirJuego = control.estadoJuegoJugador();
                            final String lugar = seleccionarLugarCarta();
                            if (lugar == "Juego") {
                                final int cantidadCartasJugador = baraja.obtenerCartasMesaJugador().size();
                                if(cantidadCartasJugador == 0){
                                    if (empezarJuego || seguirJuego) {
                                        ponerCartaEnMesaJugador(index);
                                    } else {
                                        mostrarMensaje("Para iniciar el juego debes poner un semaforo verde");
                                    }
                                }
                                if(cantidadCartasJugador > 0){
                                    final int cualCarta = control.atacar(baraja.obtenerCartasMesaJugador().getLast().getNombreCarta());
                                    final String carta = baraja.obtenerCartasJugador().get(index).getNombreCarta();
                                    if (cualCarta == 1) {
                                        if (carta == "SIGA" || carta =="VIALIBRE") {
                                            if (carta == "VIALIBRE") {
                                                sumarPuntosJugador(300);
                                            }
                                            ponerCartaEnMesaJugador(index);
                                        }else{
                                            mostrarMensaje("Tienes que poner un semaforo verde");
                                        }
                                    } else if (cualCarta == 2) {
                                        if (carta == "REPARACIONES" || carta == "ASDELVOLANTE") {
                                            if (carta == "ASDELVOLANTE") {
                                                sumarPuntosJugador(300);
                                            }
                                            ponerCartaEnMesaJugador(index);
                                        }else{
                                            mostrarMensaje("tienes que poner  una carta reparaciones");
                                        }
                                    } else if (cualCarta == 3) {
                                        if(carta == "25" || carta == "50"){
                                            ponerCartaEnMesaJugador(index);
                                            sumarPuntosJugador(Integer.valueOf(carta));
                                        }
                                        else if (carta == "FINLIMITE" || carta =="VIALIBRE") {
                                            if (carta == "VIALIBRE") {
                                                sumarPuntosJugador(300);
                                            }
                                            ponerCartaEnMesaJugador(index);
                                        }else{
                                            mostrarMensaje("tienes que poner una carta de distancia menor o igual 50");
                                        }
                                    } else if (cualCarta == 4) {
                                        if (carta == "RUEDARECAMBIO" || carta == "IMPINCHABLE") {
                                            if (carta == "IMPINCHABLE") {
                                                sumarPuntosJugador(300);
                                            }
                                            ponerCartaEnMesaJugador(index);
                                        }else{
                                            mostrarMensaje("Tienes que poner reparacion de rueda");
                                        }
                                    } else if (cualCarta == 5) {
                                        if (carta == "GASOLINA" || carta ==  "CISTERNA") {
                                            if (carta == "CISTERNA") {
                                                sumarPuntosJugador(300);
                                            }
                                            ponerCartaEnMesaJugador(index);
                                        }else{
                                            mostrarMensaje("tienes que poner una carta Gasolina");
                                        }
                                    }else{
                                        if (baraja.obtenerCartasJugador().get(index).getTipoCarta() == "Distancia") {
                                            sumarPuntosJugador(Integer.valueOf(baraja.obtenerCartasJugador().get(index).getNombreCarta()));
                                            control.viajeSeguroJugador(Integer.valueOf(baraja.obtenerCartasJugador().get(index).getNombreCarta()));
                                        }
                                        if (baraja.obtenerCartasJugador().get(index).getTipoCarta() == "PODER") {
                                            sumarPuntosJugador(100);
                                        }
                                        ponerCartaEnMesaJugador(index);
                                    }
                                    int contadorCartasPoder = 0;
                                    for(int tipoCarta = 0; tipoCarta < baraja.obtenerCartasMesaJugador().size(); tipoCarta++){                                        
                                        if (baraja.obtenerCartasMesaJugador().get(tipoCarta).getTipoCarta() == "PODER") {
                                            contadorCartasPoder++;
                                        }
                                    }
                                    if (contadorCartasPoder == 4) {
                                        sumarPuntosJugador(300);
                                    }

                                }
                            }
                            if (lugar == "Descarte") {
                                baraja.descartarCartaJugador(index);
                                actualizarPanelCartasDescartadas();
                                actualizarPanelComputador();
                                actualizarPanelJugador();       
                                control.yaJugoJugador();                         
                            }
                            if (lugar == "Oponente") {
                                int tieneCarta = 0;
                                for (int cualCarta = 0; cualCarta < baraja.obtenerCartasMesaComputador().size(); cualCarta++) {
                                    if (baraja.obtenerCartasMesaComputador().get(cualCarta).getNombreCarta() == "VIALIBRE") {
                                        tieneCarta = 1;
                                    }
                                    if (baraja.obtenerCartasMesaComputador().get(cualCarta).getNombreCarta() == "ASDELVOLANTE") {
                                        tieneCarta = 2;
                                    }
                                    if (baraja.obtenerCartasMesaComputador().get(cualCarta).getNombreCarta() == "CISTERNA") {
                                        tieneCarta = 3;
                                    }
                                    if (baraja.obtenerCartasMesaComputador().get(cualCarta).getNombreCarta() == "IMPINCHABLE") {
                                        tieneCarta = 4;
                                    }
                                }
                                control.ponerAtaque(baraja.obtenerCartasJugador().get(index).getTipoCarta());
                                if (control.puedeAtacar()) {
                                    if(baraja.obtenerCartasMesaComputador().size()>0){
                                        if (baraja.obtenerCartasMesaComputador().getLast().getTipoCarta() == "Ataque") {
                                            mostrarMensaje("Solamente puedes hacer un ataque a la vez");
                                        } else {
                                            if (tieneCarta == 1) {
                                                if (baraja.obtenerCartasJugador().get(index).getNombreCarta() == "PARE" ||baraja.obtenerCartasJugador().get(index).getNombreCarta() == "LIMITEVELOCIDAD") {
                                                    mostrarMensaje("No puedes poner semaforos rojos ni limites de velocidad porque tiene inmunidad");
                                                }
                                                else{
                                                    ponerCartaEnMesaComputador(index);
                                                }
                                            }
                                            else if(tieneCarta == 2){
                                                if (baraja.obtenerCartasJugador().get(index).getNombreCarta() == "ACCIDENTE") {
                                                    mostrarMensaje("No puedes poner accidentes porque tiene inmunidad");
                                                } else {
                                                    ponerCartaEnMesaComputador(index);
                                                }
                                            }
                                            else if(tieneCarta == 3){
                                                if (baraja.obtenerCartasJugador().get(index).getNombreCarta() == "SINGASOLINA") {
                                                    mostrarMensaje("No puedes poner sin gasolina porque tiene inmunidad");
                                                } else {
                                                    ponerCartaEnMesaComputador(index);
                                                }
                                            }
                                            else if(tieneCarta == 4){
                                                if (baraja.obtenerCartasJugador().get(index).getNombreCarta() == "PINCHAZO") {
                                                    mostrarMensaje("No puedes poner pinchazos porque tiene inmunidad");
                                                } else {
                                                    ponerCartaEnMesaComputador(index);
                                                }
                                            }else{
                                                ponerCartaEnMesaComputador(index);
                                            }
                                        }

                                        int contadorCartasPoder = 0;
                                        for (int tipoCarta = 0; tipoCarta < baraja.obtenerCartasMesaComputador().size(); tipoCarta++) {
                                            if (baraja.obtenerCartasMesaComputador().get(tipoCarta).getTipoCarta() == "PODER") {
                                                contadorCartasPoder++;
                                            }
                                        }
                                        if (contadorCartasPoder == 4) {
                                            sumarPuntosJugador(300);
                                        }
                                    }else{
                                        ponerCartaEnMesaComputador(index);
                                    }
                                }else{
                                    mostrarMensaje("Solamente puedes poner cartas de ataque");
                                }
                            }                                                    
                        }    
                    }                    
                }
            }
            if (control.turnoComputador()) {
                            boolean empezarJuego = false;
                            final boolean seguirJuego = control.estadoJuegoComputador();
                            baraja.darCartasComputador();
                            actualizarPanelComputador();
                            actualizarPanelCartasDisponibles();
                            final int cantidadCartasZonaJuegoComputador = baraja.obtenerCartasMesaComputador().size();
                            if (cantidadCartasZonaJuegoComputador == 0) {
                                boolean primerMovimiento = true;
                                for (int index = 0; index < baraja.obtenerCartasComputador().size(); index++) {
                                    empezarJuego = control.empezarJuego(baraja.obtenerCartasComputador().get(index).getNombreCarta());
                                    if (empezarJuego || seguirJuego) {
                                        
                                        primerMovimiento = false;
                                        baraja.ponerCartaEnMesaComputador(index, 0);
                                        actualizarPanelCartaZonaJuegoComputador();
                                        actualizarPanelComputador();
                                        control.yaJugoComputador();
                                        control.comerCarta(false);
                                        control.seguirJuegoComputador(true);
                                        break;
                                    }
                                }
                                if (primerMovimiento) {
                                    int tieneCarta = 0;
                                    for (int cual = 0; cual < baraja.obtenerCartasMesaJugador().size(); cual++) {
                                        if (baraja.obtenerCartasMesaJugador().get(cual).getNombreCarta() == "VIALIBRE") {
                                            tieneCarta = 1;
                                        }
                                        if (baraja.obtenerCartasMesaJugador().get(cual).getNombreCarta() == "ASDELVOLANTE") {
                                            tieneCarta = 2;
                                        }
                                        if (baraja.obtenerCartasMesaJugador().get(cual).getNombreCarta() == "CISTERNA") {
                                            tieneCarta = 3;
                                        }
                                        if (baraja.obtenerCartasMesaJugador().get(cual).getNombreCarta() == "IMPINCHABLE") {
                                            tieneCarta = 4;
                                        }

                                    }
                                    final String carta = baraja.obtenerCartasComputador().getFirst().getTipoCarta();
                                    if (carta == "Ataque") {
                                        if (tieneCarta == 1) {
                                            if (baraja.obtenerCartasComputador().getFirst().getNombreCarta() == "PARE"|| baraja.obtenerCartasComputador().getFirst().getNombreCarta() == "LIMITEVELOCIDAD") {
                                                descartarCarta();
                                            } else {
                                                ponerCartaEnMesaJugador();
                                            }
                                        } else if (tieneCarta == 2) {
                                            if (baraja.obtenerCartasComputador().getFirst().getNombreCarta() == "ACCIDENTE") {
                                                descartarCarta();
                                            } else {
                                                ponerCartaEnMesaJugador();
                                            }
                                        } else if (tieneCarta == 3) {
                                            if (baraja.obtenerCartasComputador().getFirst().getNombreCarta() == "SINGASOLINA") {
                                                descartarCarta();
                                            } else {
                                                ponerCartaEnMesaJugador();
                                            }
                                        } else if (tieneCarta == 4) {
                                            if (baraja.obtenerCartasComputador().getFirst().getNombreCarta() == "PINCHAZO") {
                                                descartarCarta();
                                            } else {
                                                ponerCartaEnMesaJugador();
                                            }
                                        } else {
                                            ponerCartaEnMesaJugador();
                                        }
                                    }else{
                                        
                                        baraja.descartarCartaComputador();
                                        actualizarPanelCartasDescartadas();
                                        actualizarPanelComputador();
                                        control.yaJugoComputador();
                                        control.comerCarta(false);
                                    }
                                }
                            }
                            if(cantidadCartasZonaJuegoComputador > 0) {
                                final int cualCarta = control.atacar(baraja.obtenerCartasMesaComputador().getLast().getNombreCarta());
                                if (cualCarta == 1) {
                                    responderComputador("SIGA", "VIALIBRE", cualCarta);
                                }
                                else if (cualCarta == 2) {
                                    responderComputador("REPARACIONES", "ASDELVOLANTE", cualCarta);
                                }
                                else if (cualCarta == 3) {
                                    responderComputador("FINLIMITE", "VIALIBRE", cualCarta);
                                }
                                else if (cualCarta == 4) {
                                    responderComputador("RUEDARECAMBIO", "IMPINCHABLE", cualCarta);
                                }
                                else if (cualCarta == 5) {
                                    responderComputador("GASOLINA", "CISTERNA", cualCarta);
                                }
                                else{
                                    int tieneCarta = 0;
                                    for (int cual = 0; cual < baraja.obtenerCartasMesaJugador().size(); cual++) {
                                        if (baraja.obtenerCartasMesaJugador().get(cual).getNombreCarta() == "VIALIBRE") {
                                            tieneCarta = 1;
                                        }
                                        if (baraja.obtenerCartasMesaJugador().get(cual).getNombreCarta() == "ASDELVOLANTE") {
                                            tieneCarta = 2;
                                        }
                                        if (baraja.obtenerCartasMesaJugador().get(cual).getNombreCarta() == "CISTERNA") {
                                            tieneCarta = 3;
                                        }
                                        if (baraja.obtenerCartasMesaJugador().get(cual).getNombreCarta() == "IMPINCHABLE") {
                                            tieneCarta = 4;
                                        }

                                    }
                                    final String carta = baraja.obtenerCartasComputador().getFirst().getTipoCarta();
                                    if (carta == "Ataque") {
                                        if (tieneCarta == 1) {
                                            if (baraja.obtenerCartasComputador().getFirst().getNombreCarta() == "PARE" || baraja.obtenerCartasComputador().getFirst().getNombreCarta() == "LIMITEVELOCIDAD") {
                                                descartarCarta();
                                            } else {
                                                ponerCartaEnMesaJugador();
                                            }
                                        } else if (tieneCarta == 2) {
                                            if (baraja.obtenerCartasComputador().getFirst().getNombreCarta() == "ACCIDENTE") {
                                                descartarCarta();
                                            } else {
                                                ponerCartaEnMesaJugador();
                                            }
                                        } else if (tieneCarta == 3) {
                                            if (baraja.obtenerCartasComputador().getFirst().getNombreCarta() == "SINGASOLINA") {
                                                descartarCarta();
                                            } else {
                                                ponerCartaEnMesaJugador();
                                            }
                                        } else if (tieneCarta == 4) {
                                            if (baraja.obtenerCartasComputador().getFirst().getNombreCarta() == "PINCHAZO") {
                                                descartarCarta();
                                            } else {
                                                ponerCartaEnMesaJugador();
                                            }
                                        } else {
                                            ponerCartaEnMesaJugador();
                                        }
                                    }else{
                                        if (carta == "Distancia") {
                                            sumarPuntosComputador(Integer.valueOf(baraja.obtenerCartasComputador().getFirst().getNombreCarta()));
                                            control.viajeSeguroComputador(Integer.valueOf(baraja.obtenerCartasComputador().getFirst().getNombreCarta()));
                                        }
                                        
                                        baraja.ponerCartaEnMesaComputador(0, 0);
                                        actualizarPanelCartaZonaJuegoComputador();
                                        actualizarPanelComputador();
                                        control.yaJugoComputador();
                                        control.comerCarta(false);
                                        control.seguirJuegoComputador(true);
                                    }
                                }
                                
                            }
            }
        }
    }
}

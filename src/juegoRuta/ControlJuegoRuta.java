/**
 * Archivo: PrincipalJuegoRuta.java
 * Fecha: Abril de 2019
 * @author Jaimen Aza, cod 1923556
 * @author Valentina Salamanca, cod 1842427
 */
package juegoRuta;

/**
 * The Class ControlJuegoRuta. Clase que maneja los procesos en la clase
 * GUIJuegoRuta.
 */
public class ControlJuegoRuta {

    /** The turnoJugador. */
    private boolean turnoJugador;

    /** The turnoComputador. */
    private boolean turnoComputador;

    /** The empezarJuego. */
    private boolean empezarJuego;

    /** The finJuego. */
    private boolean finJuego;

    /** The seguirJuegoJugador. */
    private boolean seguirJuegoJugador;

    /** The seguirJuegoComputador. */
    private boolean seguirJuegoComputador;

    /** The comerCarta. */
    private boolean comerCarta;

    /** The puedeAtacar. */
    private boolean puedeAtacar;

    private int kilomentrosComputador;
    private int kilomentrosJugador;

    /** The kmJugador. */
    private int kmJugador;

    /** The tkmComputador. */
    private int kmComputador;

    /** The estadoKmJugador. */
    private boolean estadoKmJugador;

    /** The estadoKmComputador. */
    private boolean estadoKmComputador;

    /** The primerMovimiento. */
    private boolean primerMovimiento;
    /**
     * Constructor de la clase ControlJuegoRuta.
     */
    public ControlJuegoRuta() {

        kmComputador = 0;
        kmJugador = 0;
        kilomentrosComputador = 0;
        kilomentrosJugador = 0;
        turnoJugador = true;
        turnoComputador = false;
        empezarJuego = false;
        finJuego = false;
        seguirJuegoJugador = false;
        seguirJuegoComputador = false;
        estadoKmJugador = false;
        estadoKmComputador = false;
        puedeAtacar = false;
        comerCarta = false;
        primerMovimiento = false;
    }

    /**
     * Metodo para saber si comio una carta el jugador o computador.
     */
    public void comerCarta(Boolean yaComio) {
        if (yaComio) {
            comerCarta = true;
        }
        if (!yaComio) {
            comerCarta = false;
        }
    }

    /**
     * Metodo para determinar si ya jugo el jugador.
     */
    public void yaJugoJugador() {
        turnoJugador = false;
        turnoComputador = true;
    }

    /**
     * Metodo para determinar si ya jugo el computador.
     */
    public void yaJugoComputador() {
        turnoComputador = false;
        turnoJugador = true;
    }

    /**
     * Metodo para determinar si el jugador puede poner una carta de ataque al
     * computador.
     */
    public void ponerAtaque(String tipoCarta) {
        puedeAtacar = false;
        if (tipoCarta == "Ataque") {
            puedeAtacar = true;
        } else {
            puedeAtacar = false;
        }
    }

    /**
     * Metodo para determinar el turno del jugador.
     * 
     * @return turnoJugador.
     */
    public boolean turnoJugador() {
        return turnoJugador;
    }

    /**
     * Metodo para determinar el turno del computador.
     * 
     * @return turnoComputador.
     */
    public boolean turnoComputador() {
        return turnoComputador;
    }

    /**
     * Metodo para determinar si el jugador puede seguir jugando (puede solo poner
     * cartas en su zona de juego si ya puso un semaforo verde, de lo contrario
     * tiene que poner una carta en la zona de descarte).
     */
    public void seguirJuegoJugador(Boolean seguir) {
        if (seguir) {
            seguirJuegoJugador = true;
        }
        if (!seguir) {
            seguirJuegoJugador = false;
        }
    }

    /**
     * Metodo para determinar si el computador puede seguir jugando (puede solo
     * poner cartas en su zona de juego si ya puso un semaforo verde, de lo
     * contrario tiene que poner una carta en la zona de descarte).
     */
    public void seguirJuegoComputador(Boolean seguir) {
        if (seguir) {
            seguirJuegoComputador = true;
        }
        if (!seguir) {
            seguirJuegoComputador = false;
        }
    }

    /**
     * Metodo para determinar si ya jug� su primera jugada (pone un s�maforo verde o
     * pone una carta en la zona de descarte).
     */
    public boolean primerMovimiento() {
        return !primerMovimiento;
    }

    /**
     * Metodo para determinar si ya pus� la carta del semaforo verde para poder
     * comenzar a jugar.
     */
    public boolean empezarJuego(String nombreCarta) {
        if (nombreCarta == "SIGA") {
            empezarJuego = true;
        } else {
            empezarJuego = false;

        }
        return empezarJuego;
    }

    /**
     * Metodo para que el computador o el jugador ponga una carta de ataque y este
     * debe responder con una carta de seguridad.
     */
    public int atacar(String carta) {
        int cualCarta = 0;
        if (carta == "PARE") {
            cualCarta = 1;
        }
        if (carta == "ACCIDENTE") {
            cualCarta = 2;
        }
        if (carta == "LIMITEVELOCIDAD") {
            cualCarta = 3;
        }
        if (carta == "PINCHAZO") {
            cualCarta = 4;
        }
        if (carta == "SINGASOLINA") {
            cualCarta = 5;
            ;
        }
        return cualCarta;
    }


    /**
     * Metodo para sumar los kilometros de las cartas de distancia del jugador.
     */
    public int sumarKmJugador(int km) {
        kmJugador = kmJugador + km;
        if (kmJugador >= 1000) {
            estadoKmJugador = true;
        }
        return kmJugador;
    }

    public void viajeSeguroJugador(int km) {
        kilomentrosJugador = kilomentrosJugador+km;
        if (kilomentrosJugador >= 1000) {
            kmJugador = kmJugador+300;
        }
    }

    /**
     * Metodo para sumar los kilometros de las cartas de distancia del computador.
     */
    public int sumarKmComputador(int km) {
        kmComputador = kmComputador + km;
        if (kmComputador >= 1000) {
            estadoKmComputador = true;
        }
        return kmComputador;
    }

    public void viajeSeguroComputador(int km) {
        kilomentrosComputador = kilomentrosComputador + km;
        if (kilomentrosComputador >= 1000) {
            kmComputador = kmComputador + 300;
        }
    }
    /**
     * Metodo para determinar si ya comio
     */
    public boolean yaComioCarta() {
        return comerCarta;
    }

    /**
     * Metodo para determinar en que estado esta el jugador. Si tiene un ataque o
     * esta libre de ataque Si es la primera, solamente podra seguir si responde con
     * la carta de seguridad correspondiente. Para la segunda es saber si no tiene
     * ningun ataque.
     */
    public boolean estadoJuegoJugador() {
        return seguirJuegoJugador;
    }

    /**
     * Metodo para determinar en que estado esta el computador. Si tiene un ataque o
     * esta libre de ataque Si es la primera, solamente podra seguir si responde con
     * la carta de seguridad correspondiente. Para la segunda es saber si no tiene
     * ningun ataque.
     */
    public boolean estadoJuegoComputador() {
        return seguirJuegoComputador;
    }

    /**
     * Metodo para que el jugador o el computador pueda atacar al contricante.
     */
    public boolean puedeAtacar() {
        return puedeAtacar;
    }

    /**
     * Metodo para determinar cuantos kilomentros lleva el jugador.
     */
    public boolean estadokmJugador() {
        return estadoKmJugador;
    }

    /**
     * Metodo para determinar cuantos kilomentros lleva el computador.
     */
    public boolean estadokmComputador() {
        return estadoKmComputador;
    }

    public int totalPuntosJugador() {
        return kmJugador;
    }

    public int totalPuntosComputador() {
        return kmComputador;
    }
}
